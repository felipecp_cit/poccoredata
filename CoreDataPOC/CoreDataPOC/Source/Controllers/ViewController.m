//
//  ViewController.m
//  CoreDataPOC
//
//  Created by Felipe Carvalho Pignolatti on 05/07/18.
//  Copyright © 2018 Felipe Pignolatti. All rights reserved.
//

#import "ViewController.h"
#import "CategoryModelProvider.h"
#import "CategoryProvider.h"

static NSInteger const kCategoriesCount = 10;

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - Cycle view

- (void)viewDidLoad {
    [super viewDidLoad];
    [self saveCategories];
}

#pragma mark - Private methods

- (void)saveCategories {
    NSError *error;
    [CategoryProvider saveArray:[self getCategoriesList] withError:error];
    
    if (error) {
        NSLog(@"Error: %@", error);
    }
}

- (NSArray<CategoryModelProvider *> *)getCategoriesList {
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < kCategoriesCount; i++) {
        CategoryModelProvider *current = [CategoryModelProvider new];
        current.categoryId = [@(i) stringValue];
        current.name = [NSString stringWithFormat:@"Category name %@", [@(i) stringValue]];
        
        [array addObject:current];
    }
    
    return array;
}

@end
