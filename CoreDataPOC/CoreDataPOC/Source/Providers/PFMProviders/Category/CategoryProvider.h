//
//  CategoryProvider.h
//  CoreDataPOC
//
//  Created by Felipe Carvalho Pignolatti on 06/07/18.
//  Copyright © 2018 Felipe Pignolatti. All rights reserved.
//

@import Foundation;
#import "CategoryModelProvider.h"
#import "BaseProvider.h"

@interface CategoryProvider : BaseProvider

/**
 *  Método responsável por salvar uma categoria no banco de dados
 *
 *  @param categoryModel    Model de categoria
 *  @param error            Error a ser passado por referência
 */
+ (void)save:(CategoryModelProvider *)categoryModel withError:(NSError *)error;

/**
 *  Método responsável por salvar uma lista de categorias no banco de dados
 *
 *  @param categoryModelList    Lista de models de categoria
 *  @param error                Error a ser passado por referência
 */
+ (void)saveArray:(NSArray<CategoryModelProvider *> *)categoryModelList withError:(NSError *)error;

/**
 *  Método responsável por consultar todas as categorias.
 *
 *  @param predicateFormat  Formato de filtro e seus valores
 */
+ (NSArray<CategoryModelProvider *> *)getAll:(NSString *)predicateFormat, ...;

/**
 *  Método responsável por consultar uma categoria no banco de dados pelo ID
 *
 *  @param categoryId   Id da categoria ser consultada
 */
+ (CategoryModelProvider *)getById:(NSString *)categoryId;

@end
