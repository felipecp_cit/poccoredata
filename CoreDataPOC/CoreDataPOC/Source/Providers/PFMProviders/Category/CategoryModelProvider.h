//
//  CategoryModelProvider.h
//  CoreDataPOC
//
//  Created by Felipe Carvalho Pignolatti on 06/07/18.
//  Copyright © 2018 Felipe Pignolatti. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryModelProvider : NSObject
@property (strong, nonatomic) NSString *categoryId;
@property (strong, nonatomic) NSString *name;
@end
