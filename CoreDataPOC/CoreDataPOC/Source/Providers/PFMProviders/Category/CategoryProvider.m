//
//  CategoryProvider.m
//  CoreDataPOC
//
//  Created by Felipe Carvalho Pignolatti on 06/07/18.
//  Copyright © 2018 Felipe Pignolatti. All rights reserved.
//

#import "CategoryProvider.h"

static NSString *const kCategoryEntityName = @"Category";
static NSString *const kCategoryIdPropertyName = @"categoryId";
static NSString *const kCategoryNamePropertyName = @"name";
static NSString *const kPredicateCategoryIdFilter = @"categoryId == %@";

@implementation CategoryProvider

#pragma mark - Public methods

+ (void)save:(CategoryModelProvider *)categoryModel withError:(NSError *)error {
    NSManagedObjectContext *context = [self getProviderContext];
    
    NSManagedObject *newDevice = [NSEntityDescription insertNewObjectForEntityForName:kCategoryEntityName inManagedObjectContext:context];
    [newDevice setValue:categoryModel.categoryId forKey:kCategoryIdPropertyName];
    [newDevice setValue:categoryModel.name forKey:kCategoryNamePropertyName];
    
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
}

+ (void)saveArray:(NSArray<CategoryModelProvider *> *)categoryModelList withError:(NSError *)error {
    if (!categoryModelList.count) {
        return;
    }
    
    for (CategoryModelProvider *current in categoryModelList) {
        [self save:current withError:error];
    }
}

+ (NSArray<CategoryModelProvider *> *)getAll:(NSString *)predicateFormat, ... {
    NSArray<CategoryModelProvider *> *result = nil;
    result = [self executeFetchRequest:predicateFormat];
    return result;
}

+ (CategoryModelProvider *)getById:(NSString *)categoryId {
    NSArray<CategoryModelProvider *> *result = nil;
    result = [self executeFetchRequest:kPredicateCategoryIdFilter, categoryId];
    return result.firstObject;
}

#pragma mark - Private methods

+ (nullable NSArray *)executeFetchRequest:(NSString *)predicateFormat, ... {
    NSManagedObjectContext *context = [self getProviderContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kCategoryEntityName];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:predicateFormat]];
    return [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
}

@end
