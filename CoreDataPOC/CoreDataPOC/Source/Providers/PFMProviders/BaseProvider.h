//
//  BaseProvider.h
//  CoreDataPOC
//
//  Created by Felipe Carvalho Pignolatti on 06/07/18.
//  Copyright © 2018 Felipe Pignolatti. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreData;

@interface BaseProvider : NSObject

+ (NSManagedObjectContext *)getProviderContext;

@end
