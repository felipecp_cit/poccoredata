//
//  CoreDataProvider.h
//  CoreDataPOC
//
//  Created by Felipe Carvalho Pignolatti on 06/07/18.
//  Copyright © 2018 Felipe Pignolatti. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreData;

@interface CoreDataProvider : NSObject
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

/**
 *  Instância única do Provider de CoreData
 */
+ (instancetype)shared;

/**
 *  Método para salvar o contexto do CoreData
 */
- (void)saveContext;

@end
