//
//  AppDelegate.h
//  CoreDataPOC
//
//  Created by Felipe Carvalho Pignolatti on 05/07/18.
//  Copyright © 2018 Felipe Pignolatti. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreData;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

