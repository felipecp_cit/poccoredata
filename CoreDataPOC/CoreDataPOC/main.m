//
//  main.m
//  CoreDataPOC
//
//  Created by Felipe Carvalho Pignolatti on 05/07/18.
//  Copyright © 2018 Felipe Pignolatti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
